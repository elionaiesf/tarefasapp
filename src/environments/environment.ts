// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,


  firebase:{

    apiKey: "AIzaSyA95Fz4zZzRB9h0UaOs8THSjZWsnLUJGkw",
    authDomain: "tarefasapp-1dc1f.firebaseapp.com",
    databaseURL: "https://tarefasapp-1dc1f.firebaseio.com",
    projectId: "tarefasapp-1dc1f",
    storageBucket: "tarefasapp-1dc1f.appspot.com",
    messagingSenderId: "706137500192",
    appId: "1:706137500192:web:3ab6ef8903a75eaba3aa45"

  }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
