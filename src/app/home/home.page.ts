import { Component } from '@angular/core';

import {NavController} from '@ionic/angular';
import { AngularFireAuth } from '@angular/fire/auth';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Router} from '@angular/router';
import { CadastrosPage } from '../cadastros/cadastros.page';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public afAuth: AngularFireAuth, public navCtrl: NavController, private router: Router) {}

  signOut(){
    this.afAuth.auth.signOut().then(()=>{

        location.reload();

    });


  }

  about(){
    this.navCtrl.navigateForward('about', {});
  }

  go(){
    this.router.navigate(['cadastros']);
  }


}
